#!../../bin/linux-x86_64/tc_mcu_26

#- You may have to change tc_mcu_26 to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/tc_mcu_26.dbd"
tc_mcu_26_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=tamaskerenyi")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=tamaskerenyi"
