require asyn, 4.33.0
require stream, 2.8.8
require tc_mcu_26, master


# -----------------------------------------------------------------------------
# IOC common settings
# -----------------------------------------------------------------------------
epicsEnvSet("ASYN_PORT",      "MCU026_PLC")
epicsEnvSet("IP_ADDR",        "172.30.244.118")
epicsEnvSet("PORT_ADDR",      "5000")
epicsEnvSet("PREFIX",         "UTG-MCU-026")
epicsEnvSet("STREAM_PROTOCOL_PATH", $(tc_mcu_26_DIR)db)

# Connection
drvAsynIPPortConfigure("$(ASYN_PORT)","$(IP_ADDR):$(PORT_ADDR)",0,0,0)
# Loading database
dbLoadRecords(detPlcutilTimestamp.db, "P=$(PREFIX), PORT=$(ASYN_PORT), ADDR=01")

# Start the IOC
iocInit
